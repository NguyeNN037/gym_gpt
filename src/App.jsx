import Gymapp from "./Gymapp/Hompage/Gymapp.jsx";
import Workout from "./Gymapp/Workout/Workout.jsx";
import { BrowserRouter, Routes, Route } from "react-router-dom";
function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Gymapp />} />
          <Route path="/exercise" element={<Workout />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
