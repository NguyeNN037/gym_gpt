import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  planExercise: {},
};

export const planSlice = createSlice({
  name: "plan",
  initialState,
  reducers: {
    addPlan: (state, action) => {
      state.planExercise = action.payload;
    },
  },
});

export const { addPlan } = planSlice.actions;

export default planSlice.reducer;
