import { configureStore } from "@reduxjs/toolkit";
import planSlice from "./planSlice";

export const storeRedux = configureStore({
  reducer: {
    plan: planSlice,
  },
});
