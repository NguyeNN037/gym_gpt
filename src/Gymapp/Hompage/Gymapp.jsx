import React from "react";
import { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Form, Button, Input } from "antd";
import { NavLink } from "react-router-dom";

import * as ele from "../Element/Element.jsx";
import { addPlan } from "../../Redux/planSlice";

import "./Gymapp.css";

export default function Gymapp() {
  //store plan to the redux through planSlice
  const plan = useSelector((state) => state.plan.planExercise);

  const dispath = useDispatch();
  const [answer, setAnswer] = useState("");
  console.log("Plan", plan);

  const onFinish = async (values) => {
    const { timeGym, equipment, muscle, fitness_level, fitness_goals } = values;
    const response = await fetch("http://localhost:3080/workout", {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        time: timeGym,
        equipment: equipment,
        muscle: muscle,
        fitness_level: fitness_level,
        fitness_goals: fitness_goals,
      }),
    }).catch((error) => {
      console.log("error", error);
    });

    if (!response.ok) {
      console.log("response: ", response);
      console.log("Error");
      console.log(values);
      return;
    }
    // Get the result from BE
    const data = await response.json();
    console.log("data: ", data);
    //Store it first to the Redux store
    dispath(addPlan(data));
    //Make change state in ReactComponent
    setAnswer(data);

    console.log(plan);
  };

  return (
    <div className="container">
      <div>
        <ele.Navbar />
      </div>
      <div className="homePage">
        <div className="inputData">
          <div className="section-title">Gym is Life</div>
          <Form
            layout="vertical"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 12,
            }}
            style={{
              maxWidth: 600,
            }}
            onFinish={onFinish}>
            <Form.Item label="TimeGym" name="timeGym">
              <Input placeholder="Practice time for a trainning session" />
            </Form.Item>

            <Form.Item label="Equipment" name="equipment">
              <Input placeholder="Equitement" />
            </Form.Item>

            <Form.Item label="Muscle" name="muscle">
              <Input placeholder="muscle" />
            </Form.Item>

            <Form.Item label="Fitness_level" name="fitness_level">
              <Input placeholder="fitness_level" />
            </Form.Item>

            <Form.Item label="Fitness_goals" name="fitness_goals">
              <Input placeholder="fitness_goals" />
            </Form.Item>

            <Form.Item>
              <Button className="inputButton" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>

        <div className="answer-gpt">
          <div className="answer-title">Your plan here</div>
          <div className="plans-container">
            {Object.keys(answer).map((section) => (
              <div key={section} className="plan">
                <h3 className="section-title">{section}</h3>
                {answer[section] &&
                  answer[section].map(
                    (exercise, index) =>
                      exercise && (
                        <div className="exercise-format" key={index}>
                          <p className="exercise-name">
                            Exercise: {exercise.exericse || "N/A"}
                          </p>
                          <div className="exercise-details">
                            <p>Sets: {exercise.sets || "N/A"}</p>
                            <p>Reps: {exercise.reps || "N/A"}</p>
                            <p>Time: {exercise.time || "N/A"}</p>
                            <p>Break: {exercise.breakTime || "N/A"}</p>
                            {/* {exercise.video && exercise.video.url ? (
                            <div className="video">
                              <iframe
                                width="560"
                                height="315"
                                src={`https://www.youtube.com/embed/${exercise.video.id}`}
                                frameBorder="0"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                allowFullScreen></iframe>
                            </div>
                          ) : (
                            <p>Video: N|A</p>
                          )} */}
                          </div>
                        </div>
                      )
                  )}
              </div>
            ))}
          </div>

          <div className="download-plan">
            <a href="http://localhost:3080/export-excel">
              <button>Download your plan</button>
            </a>

            <button>
              <NavLink to="/exercise">Let's Workout</NavLink>
            </button>

          </div>
        </div>
      </div>
      <div>
        <ele.Footer />
      </div>
    </div>
  );
}
