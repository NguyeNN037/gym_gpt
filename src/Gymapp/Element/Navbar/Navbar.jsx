import React from "react";
import { NavLink } from "react-router-dom";
import "./Navbar.css";
export default function Navbar() {
  return (
    <nav className="navbar">
      <div className="navIcon">
        <a href="">
          <img
            src="https://www.worldgym.com/images/logos/logo-light.png"
            alt=""
          />
        </a>
      </div>
      <div className="navItems">
        <ul>
          <li>
            <NavLink exact to="/">
              Gym
            </NavLink>
          </li>

          <li>
            <NavLink to="/exercise" relavtive="path">
              Exericse
            </NavLink>
          </li>

          <li>
            <NavLink to="/exercise">Training</NavLink>
          </li>

          <li>
            <NavLink exact to="/">
              About
            </NavLink>
          </li>
        </ul>
      </div>
    </nav>
  );
}
