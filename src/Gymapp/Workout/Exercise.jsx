import React from "react";
import { useState, useEffect } from "react";
export default function Exercise({ exercise }) {
  const [timer, setTimer] = useState(exercise.time);
  const [started, setStarted] = useState(false);
  const [completed, setCompleted] = useState(false);
  const [breakTime, setBreakTime] = useState(exercise.breakTime);
  const [onBreak, setOnBreak] = useState(false);

  useEffect(() => {
    let intervalId;

    if (started && timer > 0) {
      intervalId = setInterval(() => {
        setTimer((timer) => timer - 1);
      }, 1000);
    } else if (started && timer === 0 && !completed) {
      setCompleted(true);
      setOnBreak(true);
      setStarted(false);
    }

    if (onBreak && breakTime > 0) {
      intervalId = setInterval(() => {
        setBreakTime((breakTime) => breakTime - 1);
      }, 1000);
    } else if (onBreak && breakTime === 0) {
      setOnBreak(false);
      setStarted(true);
    }

    return () => clearInterval(intervalId);
  }, [started, completed, timer, onBreak, breakTime]);

  return (
    <div>
      <h2 style={{ color: "yellow" }}> Name: {exercise.exericse}</h2>
      <p>Sets: {exercise.sets}</p>
      <p>Reps: {exercise.reps}</p>
      <iframe
        width="560"
        height="315"
        src={`https://www.youtube.com/embed/${exercise.video.id}`}
        frameBorder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen></iframe>
      {!onBreak && <p>Time remaining: {timer > 0 ? timer : "0"}</p>}
      {!completed && !onBreak && (
        <button onClick={() => setStarted(true)}>START</button>
      )}
      {completed && <p>Exercise Completed</p>}
      {/* Here, the break time will still be displayed after the break is over */}
      {onBreak && (
        <p>Break Time remaining: {breakTime > 0 ? breakTime : "0"}</p>
      )}
    </div>
  );
}
