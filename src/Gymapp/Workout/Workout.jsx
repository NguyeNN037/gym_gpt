import React from "react";
import "./Workout.css";
import { useSelector } from "react-redux";
import Exercise from "./Exercise";
import * as ele from "../Element/Element.jsx";

export default function Workout() {
  const plan = useSelector((state) => state.plan.planExercise);
  console.log("plan: ", plan);

  return (
    <div>
      <div>
        <ele.Navbar />
      </div>
      <div className="exerciseTraining">
        {plan && Object.keys(plan).length > 0 ? (
          Object.keys(plan).map((section) => (
            <div>
              <h2 className="section-title">{section}</h2>
              <div key={section} className="exercises-container">
                {plan[section].map((exercise, index) => (
                  <div key={index} className="exercise">
                    <Exercise
                      className="exercise-item"
                      exercise={exercise}
                      key={index}
                    />
                  </div>
                ))}
              </div>
            </div>
          ))
        ) : (
          <div>
            <h1>There is no Plan</h1>
          </div>
        )}
      </div>
      <div>
        <ele.Footer />
      </div>
    </div>
  );
}
