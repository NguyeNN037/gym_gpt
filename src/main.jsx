import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import { storeRedux } from "./Redux/storeRedux.jsx";
import { Provider } from "react-redux";

ReactDOM.createRoot(document.getElementById("root")).render(
  <Provider store={storeRedux}>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </Provider>
);
