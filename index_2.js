import express from "express";
import { Configuration, OpenAIApi } from "openai";

const app = express();

const configuration = new Configuration({
  apiKey: "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",
});

const openai = new OpenAIApi(configuration);

app.get("/workout", async (req, res) => {
  try {
    const [time, equipment, muscle, fitness_level, fitness_goals] = [
      "30 minutes",
      "dumbbells",
      "biceps",
      "beginner",
      "strength",
    ];

    const prompt = `Given a user with fitness level ${fitness_level}, 
        with a goal of ${fitness_goals}, having ${equipment} equipment, 
        focusing on ${muscle} muscle group, and ${time} for a workout, 
        create a workout plan with break, warm-up, main Exercises, and cool-down phases.Between 2 exercises or 2 phases, there will a breaktime`;

    const gptResponse = await openai.createCompletion({
      model: "text-davinci-003",
      prompt: prompt,
      max_tokens: 300,
      temperature: 0.3,
    });

    const rawOutput = gptResponse.data.choices[0].text.trim();

    // Format datav Durock POM Piano
    const sections = rawOutput.split(/\n(?=[A-Z])/);
    const formattedOutput = {};

    for (let section of sections) {
      // tách mảng title và bài tập trước ":" là title, sau là mảng bài tập
      const [title, ...exercises] = section.split(":");
      const formattedExercises = exercises[0].split(", ").map((exercise) => {
        const [name, time] = exercise.split(" for ");
        console.log("name: ", name);
        console.log("time: ", time);
        return {
          exercise: name.trim(),
          time: time ? time.replace(" minutes.", "").trim() : undefined,
        };
      });

      formattedOutput[title.toLowerCase().replace(" ", "_")] =
        formattedExercises;
    }
    res.json(formattedOutput);
    console.log("formattedOutput: ", formattedOutput);
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Server running on port ${port}`));
