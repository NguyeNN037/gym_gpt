import { Configuration, OpenAIApi } from "openai";
import * as lb from "./Library/Library.js";
import express from "express";
import cors from "cors";
import path from "path";
import { format } from "util";

const app = express();
const port = 3080;

app.use(cors());
app.use(express.json());

const configuration = new Configuration({
  organization: "org-DlSJqlnbaUsGhiKzhq7kieyS",
  apiKey: "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",
});

const openai = new OpenAIApi(configuration);

let lastestWorkoutPlan = {};

app.post("/workout", async (req, res) => {
  try {
    const { time, equipment, muscle, fitness_level, fitness_goals } = req.body;

    const prompt = `Given a user with a fitness level of ${fitness_level},
    a goal of ${fitness_goals}, ${equipment} equipment available,
    focusing on the ${muscle} muscle group, and ${time} allocated for a workout,
    create a workout plan must have warm-up, main exercises, and cool-down phases.
    For each exercise, provide the name, sets, repetitions, time, and break between two sets.
    Format each exercise like this: "Exercise: [exercise name], Sets: [number of sets], 
    Reps: [number of reps], Time: [time], Break : [time]"
    Please generate such a workout plan. `;

    const gptResponse = await openai.createCompletion({
      model: "text-davinci-003",
      prompt: prompt,
      max_tokens: 1500,
      temperature: 0.8,
    });

    const rawOutput = gptResponse.data.choices[0].text.trim();
    console.log("rawOutput: ", rawOutput);

    const extractData = (infor) => {
      return infor?.split(": ")[1];
    };

    const extractExerciseData = async (exercise) => {
      const [fullExericseName, sets, reps, time, breakTime] =
        exercise.split(", ");
      // const name = fullExericseName.replace("Exercise: ", "").trim();
      const video = await lb.searchYoutubeVideo(fullExericseName);
      return {
        exericse: extractData(fullExericseName),
        sets: extractData(sets),
        reps: extractData(reps),
        time: extractData(time),
        breakTime: extractData(breakTime),
        video: video,
      };
    };

    const formatOutput = async (data) => {
      const sections = data.split("\n\n");
      const formatOutput = {};
      for (const section of sections) {
        console.log("section: ", section);
        const [sectionTitle, ...exercises] = section.split("\n");
        console.log("exercises: ", exercises);
        formatOutput[sectionTitle] = await Promise.all(
          exercises.map(extractExerciseData)
        );
      }
      return formatOutput;
    };

    lastestWorkoutPlan = await formatOutput(rawOutput);

    console.log("lastestWorkoutPlan: ", lastestWorkoutPlan);

    res.status(200).json(lastestWorkoutPlan);
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

app.get("/export-excel", async (req, res) => {
  try {
    const filePath = await lb.createWorkoutPlanExcel(lastestWorkoutPlan);

    res.setHeader(
      "Content-Type",
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    );
    res.setHeader(
      "Content-Disposition",
      `attachment; filename=${path.basename(filePath)}`
    );

    // res.sendFile(filePath);
    res.download(filePath, "workout-plan.xlsx", function (err) {
      if (err) {
        console.log(err);
      } else {
        console.log("File downloaded");
      }
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

app.listen(port, () => console.log(`Server running on port ${port}`));
