import axios from "axios";

const apiKey = "AIzaSyAWJ7lYGRbCrIm1NbZ-QBV7v4agL7RCLP0";

const searchYoutubeVideo = async (videoName) => {
  try {
    const response = await axios.get(
      `https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=1&q=${encodeURIComponent(
        videoName
      )}&type=video&key=${apiKey}`
    );
    if (response.data.items.length > 0) {
      return {
        id: response.data.items[0].id.videoId,
        url: `https://www.youtube.com/watch?v=${response.data.items[0].id.videoId}`,
      };
    } else {
      return null;
    }
  } catch (error) {
    console.log("Error", error);
    return null;
  }
};

export default searchYoutubeVideo;
