import ExcelJS from "exceljs";
import path, { dirname } from "path";
import { fileURLToPath } from "url";

const __fileName = fileURLToPath(import.meta.url);
const __dirname = dirname(__fileName);

const createWorkoutPlanExcel = async (workoutData) => {
  const workbook = new ExcelJS.Workbook();
  const worksheet = workbook.addWorksheet("Workout Plan");

  const header = [
    "Section",
    "Exercise",
    "Sets",
    "Reps",
    "Time",
    "Break",
    "Video",
  ];

  worksheet.addRow(header);

  for (const section in workoutData) {
    if (
      Array.isArray(workoutData[section]) &&
      workoutData[section].length > 0
    ) {
      for (const exercise of workoutData[section]) {
        const row = [
          section,
          exercise.exercise,
          exercise.sets,
          exercise.reps,
          exercise.time,
          exercise.Break,
          exercise.video.url,
        ];
        worksheet.addRow(row);
      }
    } else {
      console.log(`Section "${section}" does not contain valid exercise data.`);
    }
  }

  workbook.creator = "YourApp";
  const filePath = path.join(__dirname, "workout_plan.xlsx");
  await workbook.xlsx.writeFile(filePath);

  return filePath;
};

export default createWorkoutPlanExcel;
